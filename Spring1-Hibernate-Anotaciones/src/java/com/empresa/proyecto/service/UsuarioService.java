 
package com.empresa.proyecto.service;

 
import com.empresa.proyecto.hibernatePojos.Usuario;
import java.util.List;

 
public interface UsuarioService {
    
    public List<Usuario> getAll();
   public String insertarPersona(Usuario p);
   public String eliminarPersona(String ids);
   public String actualizarPersona(Usuario p);
   public Usuario getPersona(Integer idpersona);
}
