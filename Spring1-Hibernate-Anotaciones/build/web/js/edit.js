 function edit(){
     
     
     $(".edit").click(function(){
         var ides = [];
         
         $("input[name='del']:checked").each(function(){
             ides.push($(this).val());
         });
         
         if(ides.length === 0){
             alert("Debes seleccionar fila(s) para editar"); 
         }else if(ides.length === 1){
             if(confirm("¿Desea modificar los productos seleccionados?")){
                 window.location = "../app/accion=edit?id="+ides.toString();
            }
         }else if(ides.length > 1){
             alert("Debes seleccionar solo un producto para editarlo"); 
         } 
             
     });
     
 }
