package com.empresa.proyecto.daoImplement;

import com.empresa.proyecto.hibernatePojos.Producto;
import java.util.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.springframework.stereotype.Repository;
import com.empresa.proyecto.dao.ProductoDao;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Repository//se agrega en dao para cuando alguien solicite entregue la implementacion y al que la solicita se agrega la inyeccion de dependencia 
public class ProductoDaoImplement implements ProductoDao {

    // private  SessionFactory sessionFactory; 
    private Session s;
    private Transaction tx;
    //IoC (Patron Inversion de control, tecnica inyecccion de dependencias) 

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.s = sessionFactory.openSession();
    }

    @Override
    //@Transactional //gstiona las transacciones utilizando gerCurrentSession()
    public List<Producto> getAll() {
        /* @SuppressWarnings("unchecked")   //otra forma con getCurrentSession()
        List<Producto> listProducto = (List<Producto>) sessionFactory.getCurrentSession()
                .createCriteria(Producto.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();*/

        String sql = "from Producto";
        Query query = s.createQuery(sql);
        List<Producto> list = query.list();

        return list;
    }

    @Override
    public String insertarProducto(Producto p) {

        String result = null;
        tx = s.beginTransaction();
        try {
            s.persist(p);
            tx.commit();
            result = "Creaste un nuevo producto, se vera reflejado en breve";
        } catch (Exception e) {
            result = e.getMessage();
            tx.rollback();
        }
        return result;
    }

    @Override
    public String eliminarProducto(String ids) {
        String mensaje = null;
        tx = s.beginTransaction();
        String sql = "delete Producto p where p.idproducto = :id";
        Query query = s.createQuery(sql);
        String[] id = ids.split(",");

        try {
            for (int i = 0; i < id.length; i++) {
                Integer idsInt = Integer.valueOf(id[i]);
                query.setInteger("id", idsInt);
                int resultado = query.executeUpdate();

                if (resultado == 0) {
                    mensaje = "Error, no se eliminaron los datos";
                }
            }
            tx.commit();
        } catch (Exception e) {
            mensaje = e.getMessage();
            tx.rollback();
        }  

        return mensaje = "Producto eliminado con exito";
    }

    @Override
    public String actualizarProducto(Producto p) {
        String result = null;
        tx = s.beginTransaction();
        try {
            s.clear();
            s.update(p);
            tx.commit(); 
        } catch (Exception e) {
            result = e.getMessage();
            tx.rollback();
        }
        return result= "Se modifico con exito el producto";
    }

    @Override
    public Producto getProducto(Integer idproducto) {
        Producto producto  = (Producto)s.get(Producto.class, idproducto);
        return producto;
    }

}
