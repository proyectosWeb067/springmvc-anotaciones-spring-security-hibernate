<%-- 
    Document   : index
    Created on : 13/02/2019, 04:51:29 PM
    Author     : yio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css" />"  media="screen,projection"/>
        <title>Login</title>
        <style>
        .error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#login-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border: 1px solid #000;
}
</style>
    </head>
    <body onload='document.loginForm.username.focus();'>
       
        <main class="container " >
             
        <h2 style="text-align: center;">Iniciar Sesión</h2>
            <div class="col s6 offset-s3" id="login-box">
                <div class="card-panel"> 
                    <form name='loginForm' action="<c:url value='/j_spring_security_check' />" method="POST">
                        
                        <div class="center-align ">
                            <label for="username">Nombre</label>
                            <input type="text" id="username" name="username" required="true"/>
                        </div><br/><br/>

                        <div  class="center-align" >
                            <label for="password">Contraseña</label>
                            <input type="password" id="password" name="password" required="true"/>
                        </div><br/><br/>
                        <div class="center-align">
                            <button type="submit" class="btn">Iniciar sesion</button>
                        </div><br/><br/> 
                        
                        <c:if test="${not empty error}">
                            <div class="error">${error}</div>
                        </c:if>
                        <c:if test="${not empty msg}">
                            <div class="msg">${msg}</div>
                        </c:if>
                         <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                         
                    </form> 
                </div>   
            </div>
        </main>
    </body>
</html>
