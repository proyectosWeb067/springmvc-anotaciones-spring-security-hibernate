/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.empresa.proyecto.dao.ProductoDao;
import com.empresa.proyecto.daoImplement.ProductoDaoImplement;
import com.empresa.proyecto.hibernatePojos.Producto;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author juan
 */
public class ProductoTest {
    
    
    static ProductoDao pdi;
    public ProductoTest() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Iniciando test....");
    }
    
    @After
    public void tearDown() {
        System.out.println("El test a finalizado.");
    }
    
    @Test
    @Transactional
    public void obtenerProductos(){
        pdi = new ProductoDaoImplement();
        List<Producto> l  = pdi.getAll();
        
         assertNotNull("Tiene informacion", l);
    }
}
