<%-- 
    Document   : 403
    Created on : 24/02/2019, 09:55:13 PM
    Author     : yio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error-403</title>
    </head>
    <body>
        <h1>HTTP Status 403 - Acceso Denegado</h1>

	<c:choose>
		<c:when test="${empty username}">
		  <h2>Tu no tienes permisos para ingresar a esta sección!</h2>
		</c:when>
		<c:otherwise>
		  <h2>Hola ${username} <br/>
                    Tu no tienes permisos para acceder a esta sección!</h2>
                    <a href="../app/Inicio">Regresar</a>
		</c:otherwise>
	</c:choose>

    </body>
</html>
