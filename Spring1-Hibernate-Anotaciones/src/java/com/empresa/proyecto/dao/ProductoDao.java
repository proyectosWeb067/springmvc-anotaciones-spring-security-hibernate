 
package com.empresa.proyecto.dao;
 
 
import com.empresa.proyecto.hibernatePojos.Producto;
import java.util.List;

 
public interface ProductoDao {
    
   public List<Producto> getAll();
   public String insertarProducto(Producto p);
   public String eliminarProducto(String ids);
   public String actualizarProducto(Producto p);
   public Producto getProducto(Integer idproducto);
   
    
}
