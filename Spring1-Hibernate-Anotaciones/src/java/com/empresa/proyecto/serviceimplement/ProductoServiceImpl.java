 
package com.empresa.proyecto.serviceimplement;
import com.empresa.proyecto.dao.ProductoDao;
import com.empresa.proyecto.service.ProductoService;
import com.empresa.proyecto.hibernatePojos.Producto;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

 @Service
public class ProductoServiceImpl implements ProductoService{
    
//inyectando interfaz ProductoDao a el servicio     
    @Autowired
    private ProductoDao pdao; 
    
    @Override
    public List<Producto> getAll() {
        List<Producto>  l = pdao.getAll();
        return l;
    }

    @Override
    public String insertarProducto(Producto p) {
        String m = pdao.insertarProducto(p);
             
        return m;
    }

    @Override
    public String eliminarProducto(String ids) {
        String res =  pdao.eliminarProducto(ids);
         return res;
    }

    @Override
    public String actualizarProducto(Producto p) {
        String res = pdao.actualizarProducto(p);
        return res;
    }

    @Override
    public Producto getProducto(Integer idproducto) {
        Producto p = pdao.getProducto(idproducto);
        return p;
    }
    
}
