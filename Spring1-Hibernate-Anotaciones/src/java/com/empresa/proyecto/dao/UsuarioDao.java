 
package com.empresa.proyecto.dao;
  
import com.empresa.proyecto.hibernatePojos.Usuario;
import java.util.List;

 
public interface UsuarioDao {
    
   public List<Usuario> getAll();
   public String insertarPersona(Usuario u);
   public String eliminarPersona(String ids);
   public String actualizarPersona(Usuario u);
   public Usuario getPersona(Integer idusuario);
}
