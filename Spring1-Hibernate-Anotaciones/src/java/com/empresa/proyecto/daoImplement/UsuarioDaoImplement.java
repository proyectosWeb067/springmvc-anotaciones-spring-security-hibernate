
package com.empresa.proyecto.daoImplement;
 
import com.empresa.proyecto.dao.UsuarioDao; 
import com.empresa.proyecto.hibernatePojos.Usuario;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction; 
  
import org.springframework.stereotype.Repository;


@Repository
public class UsuarioDaoImplement implements UsuarioDao {

    Session session;
    Transaction tx;
    
   
    public void setSessionFactory(SessionFactory sessionFactory){
        session = sessionFactory.openSession();
    }
    
    @Override
    public List<Usuario> getAll() {
        String sql = "from Usuario";
        Query query = session.createQuery(sql);
        List<Usuario> list = query.list();

        return list;          
    }

    @Override
    public String insertarPersona(Usuario u) {
       String result = null;
        tx = session.beginTransaction();
        try {
            session.persist(u);
            tx.commit();
             
        } catch (Exception e) {
            result = e.getMessage();
            tx.rollback();
        } 

        return result="Te has registrado, en nuestro banco";
    }

    @Override
    public String eliminarPersona(String ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String actualizarPersona(Usuario u) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario getPersona(Integer idpersona) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
  
    
}
