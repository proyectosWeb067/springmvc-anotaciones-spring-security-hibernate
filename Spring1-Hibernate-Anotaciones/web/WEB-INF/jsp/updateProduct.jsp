<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/materialize.min.css"/>"  media="screen,projection"/>

        <title>Actualizando</title>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper background blue">
                <form>
                    <div class="input-field">
                        <a href="#"  class="brand-logo"><img src="<c:url value="/img/bcolombia.jpg"/>" style="max-height: 80px; max-width: 100px;"/></a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            <li><a href="Inicio">Inicio</a></li>

                        </ul>
                    </div>
                </form>
            </div>
        </nav>
        <h3>Actualizar producto</h3> 
        <main class="container">
            <div class="row ">
                <f:form modelAttribute="producto" action="accion=saveedit" method="POST">
                    <div class="row ">  
                        <div class="row col s12 ">   
                                <div class="input-field col s1">
                                    <f:label  path="idproducto" >Id</f:label> 
                                    <f:input id="idproducto" path="idproducto" readonly="true"/> 
                                </div>
                                <div class="input-field col s3">
                                    <f:label  path="titulo" >Nombre</f:label> 
                                    <f:input id="titulo" path="titulo" /> 
                                </div>
                                <div class="input-field  col s2">
                                    <f:label  path="tipo" >Tipo</f:label> 
                                    <f:input id="tipo" path="tipo" />   
                                </div>
                                <div class="input-field col s2">
                                     <f:label  path="precio" >Precio</f:label> 
                                    <f:input id="precio" path="precio" /> 
                                </div>
                                <div class="input-field col s4">
                                     <f:label  path="descripcion" >Descripcion</f:label> 
                                    <f:input id="descripcion" path="descripcion" /> 
                                </div>
                                
                        <!--forEach var="p" items="$listap}" > Pendiente utilizar para editar varios productos a la vez
                           <div class="row col s12 ">   
                                <div class="input-field col s3">
                                    <input id="titulo" type="text" name="titulo" />
                                    <label for="titulo">Titulo</label> 
                                </div>
                                <div class="input-field  col s3">
                                    <input id="tipo" type="text" name="tipo" />
                                    <label for="tipo">Tipo</label> 
                                </div>
                                <div class="input-field col s2">
                                    <input id="precio" type="text" name="precio"/>
                                    <label for="precio">Precio</label> 
                                </div>
                                <div class="input-field col s4">
                                    <input id="descripcion" type="text" name="descripcion" class="materialize-textarea" />
                                    <label for="descripcion">Descripcion</label> 
                                </div>
                            </div>     
                        <forEach-->   
                        <div class="col s3">
                            <input class="waves-effect waves-light btn" type="submit" value="Guardar"/>  
                        </div> 
                    </div>    
                </f:form>
            </div>
        </main>
        
        
        <script type="text/javascript" src="<c:url value=" https://code.jquery.com/jquery-3.2.1.min.js"/>"></script>               
        <script type="text/javascript" src="<c:url value="/js/materialize.min.js"/>"></script> 
    </body>
</html>
