 
package com.empresa.proyecto.serviceimplement;

import com.empresa.proyecto.dao.UsuarioDao; 
import com.empresa.proyecto.hibernatePojos.Usuario;
import com.empresa.proyecto.service.UsuarioService;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService{

    
    //inyeccion de dependencias. con anotacion autowired
    @Autowired
     private UsuarioDao udao;
    
 
    @Override
    public List<Usuario> getAll() {
         List<Usuario> l = udao.getAll();
           
         return l;
    }

    @Override
    public String insertarPersona(Usuario p) {
         String resp = udao.insertarPersona(p);
          
         return resp;
    }

    @Override
    public String eliminarPersona(String ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String actualizarPersona(Usuario p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario getPersona(Integer idpersona) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
