package com.empresa.proyecto.hibernatePojos;
// Generated 10/04/2019 08:42:51 PM by Hibernate Tools 4.3.1



/**
 * Usuario generated by hbm2java
 */
public class Usuario  implements java.io.Serializable {


     private Integer idusuario;
     private String nombre;
     private String apellpat;
     private String apellmat;
     private String tipodoc;
     private int numdoc;
     private int dianac;
     private String mesnac;
     private int anac;
     private String sexo;
     private int ruc;
     private String estadcivil;
     private String estadolabo;

    public Usuario() {
    }

	
    public Usuario(String nombre, String apellpat, String apellmat, String tipodoc, int numdoc, int dianac, int anac, String sexo, int ruc, String estadcivil, String estadolabo) {
        this.nombre = nombre;
        this.apellpat = apellpat;
        this.apellmat = apellmat;
        this.tipodoc = tipodoc;
        this.numdoc = numdoc;
        this.dianac = dianac;
        this.anac = anac;
        this.sexo = sexo;
        this.ruc = ruc;
        this.estadcivil = estadcivil;
        this.estadolabo = estadolabo;
    }
    public Usuario(String nombre, String apellpat, String apellmat, String tipodoc, int numdoc, int dianac, String mesnac, int anac, String sexo, int ruc, String estadcivil, String estadolabo) {
       this.nombre = nombre;
       this.apellpat = apellpat;
       this.apellmat = apellmat;
       this.tipodoc = tipodoc;
       this.numdoc = numdoc;
       this.dianac = dianac;
       this.mesnac = mesnac;
       this.anac = anac;
       this.sexo = sexo;
       this.ruc = ruc;
       this.estadcivil = estadcivil;
       this.estadolabo = estadolabo;
    }
   
    public Integer getIdusuario() {
        return this.idusuario;
    }
    
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellpat() {
        return this.apellpat;
    }
    
    public void setApellpat(String apellpat) {
        this.apellpat = apellpat;
    }
    public String getApellmat() {
        return this.apellmat;
    }
    
    public void setApellmat(String apellmat) {
        this.apellmat = apellmat;
    }
    public String getTipodoc() {
        return this.tipodoc;
    }
    
    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }
    public int getNumdoc() {
        return this.numdoc;
    }
    
    public void setNumdoc(int numdoc) {
        this.numdoc = numdoc;
    }
    public int getDianac() {
        return this.dianac;
    }
    
    public void setDianac(int dianac) {
        this.dianac = dianac;
    }
    public String getMesnac() {
        return this.mesnac;
    }
    
    public void setMesnac(String mesnac) {
        this.mesnac = mesnac;
    }
    public int getAnac() {
        return this.anac;
    }
    
    public void setAnac(int anac) {
        this.anac = anac;
    }
    public String getSexo() {
        return this.sexo;
    }
    
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public int getRuc() {
        return this.ruc;
    }
    
    public void setRuc(int ruc) {
        this.ruc = ruc;
    }
    public String getEstadcivil() {
        return this.estadcivil;
    }
    
    public void setEstadcivil(String estadcivil) {
        this.estadcivil = estadcivil;
    }
    public String getEstadolabo() {
        return this.estadolabo;
    }
    
    public void setEstadolabo(String estadolabo) {
        this.estadolabo = estadolabo;
    }




}


